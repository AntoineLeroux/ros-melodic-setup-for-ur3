Repository made for running ROS1 with Universal Robot support on Docker and on a Amd64 architecture.

For arm64 (Jetson like) please follow this link : <>

# Setup

Run `make build` first.

Then `make compose` to check and run the container.

# Launch

The folder at project root `some_ws` is bind to `/workspaces/` in container so that you can edit your code as you wish from the host.

To open a bash terminal inside the container : `make exec`

## X11 Forward for GL Apps

You must run `xhost +local:docker` in the host terminal before connecting to the container...
