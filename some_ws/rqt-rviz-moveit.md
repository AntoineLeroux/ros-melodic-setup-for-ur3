# Calibrate UR & Run UR Driver

```bash
UR_IP=192.168.118.219

roslaunch ur_calibration calibration_correction.launch robot_ip:=$UR_IP target_filename:="/workspaces/ur3_ws/ur3_calibration.yaml"

roslaunch ur_robot_driver ur3_bringup.launch robot_ip:=$UR_IP kinematics_config:="/workspaces/ur3_ws/ur3_calibration.yaml"
```

# RQT Controller

```bash
rosrun rqt_joint_trajectory_controller rqt_joint_trajectory_controller
```

# Rviz

## Launch node

```bash
roslaunch ur3_moveit_config ur3_moveit_planning_execution.launch
```

## Launch Rviz GUI

```bash
roslaunch ur3_moveit_config moveit_rviz.launch config:=true
```
