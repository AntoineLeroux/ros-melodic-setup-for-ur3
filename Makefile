path = docker

clean:
	docker system prune -a -f

build:
	cd $(path) && docker build -t ros1_melodic_ur_desktop .

rebuild:
	clean build

run:
	cd $(path) && docker run --rm ros1_melodic_ur_desktop

stop:
	docker stop ros1_melodic_ur_desktop

rm:
	docker rm ros1_melodic_ur_desktop

rerun:
	rebuild run

compose:
	docker-compose up --build -d

exec:
	docker exec -it ros1_melodic_ur_desktop bash

attach:
	docker attach ros1_melodic_ur_desktop